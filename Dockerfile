FROM golang:1.13-alpine AS go

FROM docker:19.03

RUN apk add --no-cache bash ca-certificates make

# Configure Go
COPY --from=go /usr/local/go/ /usr/local/go/
ENV GOROOT /usr/local/go
ENV GOPATH /go
ENV PATH $GOPATH/bin:$GOROOT/bin:$PATH
RUN mkdir -p "${GOPATH}/src" "${GOPATH}/bin" && chmod -R 777 "${GOPATH}"

# Install Operator SDK
ARG RELEASE_VERSION
ADD https://github.com/operator-framework/operator-sdk/releases/download/${RELEASE_VERSION}/operator-sdk-${RELEASE_VERSION}-x86_64-linux-gnu /usr/local/bin/operator-sdk 
RUN chmod +x /usr/local/bin/operator-sdk 

# Install kubectl
ARG KUBECTL_VERSION
ADD https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl
